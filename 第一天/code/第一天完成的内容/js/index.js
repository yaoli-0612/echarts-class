window.onload = async function () {
  const res = await axios({
    url: "/dashboard",
    method: "get",
    // 额外的设置请求头
    // 把登录之后的token 带到服务器
    // 接口/后端去校验 token是否正确
    headers: {
      // msg这个值 只是用来的测试的 没有任何意义
      //   msg: "good good study day day up",
      // 缓存中获取 token
      Authorization: localStorage.getItem("token"),
    },
  });
  console.log("res:", res);
};
