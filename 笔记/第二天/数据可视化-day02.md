# 数据可视化-day02

> 今天是数据可视化项目的第`2`天



## 回顾 和 目标

> 重点内容回顾 和 学习目标

### 回顾

1. vscode+git
   1. 多测试+提升熟练度

2. 公共逻辑
   1. common.js
   2. 轻提示
   3. 基地址
   4. 拦截器
   5. 用户名显示
   6. 用户登出

3. 登录
   1. 和注册类似

4. axios拦截器
   1. 请求拦截器
      1. 发送之前
         1. config-->请求的各项设置
            1. headers:请求头

   2. 响应拦截器
      1. 服务器响应回内容之后
         1. 响应成功:
         2. 响应失败:

5. gitee
   1. 备份,多设备同步
   2. 注册账号,创建仓库,保存仓库地址到本地
      1. 推送
      2. 拉取




### 目标

1. **axios**拦截器应用
   1. token携带
   2. 数据剥离
   3. 统一的异常判断
      1. 401

2. 什么是**数据可视化**'
   1. **index.html**

3. `ECharts`使用
   1. 配置不用背下来,

4. 首页数据渲染界面



## 准备工作

> 先把今天需要用到的代码准备好,通过远程仓库获取今天需要的代码

### 需求

1. 将代码克隆到本地,切换到开发分支





### 步骤:

1. 克隆
2. 推送







### 准备工作

这一节咱们通过远程仓库的方式获取了昨天上传的代码:

1. 和直接`c+v`功能是否相同?
   1. 功能是一样的
2. 这么做的目的?
   1. 强化练习,提升某个技能的熟练度





# axios拦截器应用

![image-20220805012841037](assets/image-20220805012841037.png)

## 请求拦截器应用-统一携带token

> 首先通过`axios`拦截器完成统一的`token`携带

### 需求:

1. 所有接口的请求都携带`token`







### 分析:

1. `axios`的拦截器注册添加到公共的位置,哪里?
   1. `common.js`
2. `token`从哪里获取?
   1. `localStorage.getItem('token')`









### 请求拦截器应用-统一携带token

1. `axios`的请求拦截器什么时候触发?

   1. 发送请求 之前

2. 通过请求拦截器携带`token`的优点是?

   1. 简化编码,设置一次即可
   2. 登录/注册并不需要`token`,目前的写法也会携带
      1. 登录和注册 接口**并不需要token**
      2. 就算调用接口时,额外携带了,后端也不会去获取

3. **git记录**







# 数据可视化-基本概念

> 数据可视化是什么意思呢?

### 概念

1. 借助**图形手段**, 清晰传达信息的表现方式
2. 把枯燥的数字数据, 转换成图形, **数据特点**更突出



### 举个例子

1. 哪个菜比较贵

```javascript
const arr =[
    {name:"西兰花",price:150},
    {name:"西瓜",price:230},
    {name:"西葫芦",price:224},
    {name:"西北风",price:34},
    {name:"西红柿",price:135},
    {name:"土豆",price:147},
    {name:"秋刀鱼",price:260},
]
```

![image-20220714221830487](assets/image-20220714221830487.png)





![image-20220714221910460](assets/image-20220714221910460.png)







### 数据可视化-基本概念

这一节咱们简单介绍了什么是**数据可视化**:

* 将数据转化为图形的形式,让**数据特点更突出**!!!







## 首页-顶部

> 完成顶部数据渲染

### 需求:

1. **打开页面**获取数据并渲染到页面上

![image-20220715094307540](assets/image-20220715094307540.png)



### 步骤:

1. **请求:**
   1. 页面打开发请求
      1. window.onload

   2. 逻辑较多,写到`index.js`中

2. **响应:**
   1. 数据渲染到页面上
   2. 好像**有规律**






### 核心代码





### 首页-顶部

这一节咱们完成了顶部数据的渲染,并且找到了规律简化了代码,其中用到了

1. 本节循环用到了哪个语法?
   1. `for in` 循环对象-->获取属性

2. **git记录:**



## ECharts-快速入门

> 结合文档咱们来看`ECharts`是什么,并创建一个图表出来
>
> [传送门:ECharts官网](https://echarts.apache.org/zh/index.html)

### 步骤:

1. 基于文档确认`ECharts`是个什么?
2. 基于文档创建基础图表,并调整设置查看效果?



### 测试代码

```javascript
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <!-- 1. 为 ECharts 准备一个定义了宽高的 DOM -->
    <div id="main" style="width: 600px; height: 400px"></div>
    <!-- 2.导入ECharts -->
    <script src="./lib/echarts.min.js"></script>
    <script>
      // 基于准备好的dom，初始化echarts实例
      var myChart = echarts.init(document.querySelector('#main'))

      // 指定图表的配置项和数据
      var option = {
        // 标题
        title: {
          text: '热门蔬菜排行榜!🐟',
        },
        tooltip: {},
        legend: {
          data: ['热度'], // 需要和 下面 name对应
        },
        xAxis: {
          data: ['西兰花', '西葫芦', '西北风', '西红柿', '西瓜', '西洋菜'],
        },
        yAxis: {},
        series: [
          {
            name: '热度',
            type: 'bar',
            data: [5, 20, 1, 10, 10, 20],
          },
        ],
      }

      // 使用刚指定的配置项和数据显示图表。
      myChart.setOption(option)
    </script>
  </body>
</html>

```





### ECharts-快速入门

这一节咱们基于文档测试了确认了ECharts是什么以及如何使用:

1. `ECharts`是个什么?
   1. 库
   2. 可视化图标库

2. 文档有清晰的步骤+测试代码,我们应该如何测试?
   1. 相信他->`c+v`










## ECharts-常见配置项

> 上一节有一些配置,应该如何确认含义呢?

### 步骤:

1. 根据文档检索并设置选项的含义
2. 测试上一节出现的所有选项:



### ECharts-常见配置项

- `series`：系列列表。每个系列通过 `type` 决定自己的图表类型
- `xAxis`：直角坐标系 grid 中的 x 轴
- `yAxis`：直角坐标系 grid 中的 y 轴
- `grid`：直角坐标系内绘图网格。 
- `title`：标题组件
- `tooltip`：提示框组件
- `legend`：图例组件
- `color`：调色盘颜色列表

### 核心代码:

```javascript
// 指定图表的配置项和数据
const option = {
    title: {  // 标题组件
        text: 'ECharts 入门示例', // 主标题文本
    },
    tooltip: {}, // 提示框组件
    legend: {  // 图例组件
        data: ['销量2']  // 图例的数据数组,对应series里的name
    },
    xAxis: { // 直角坐标系 grid 中的 x 轴
        data: ["衬衫", "羊毛衫", "雪纺衫", "裤子", "高跟鞋", "袜子"]
    },
    yAxis: { // 直角坐标系 grid 中的 y 轴, y轴里的data没有指定会自动从series.data里获取
    },
    series: [{
        name: '销量2', // 系列名称，用于tooltip的显示，legend 的图例筛选
        type: 'bar',  // 柱状图
        data: [5, 20, 36, 10, 10, 20] // 系列中的数据内容数组。数组项通常为具体的数据项
    }]
};
```



### ECharts-常见配置项

这一节咱们演示了常见配置项,属性很多

1. 是否需要背下来?
   1. 不需要,随用随查即可







# 薪资走势

> 首先来完成薪资走势
>
> 传送门:薪资走势

![image-20220525200044012](assets/image-20220525200044012.png)

### 基本套路:

1. 官网找到相似案例
2. 按照需求定制图表

相似案例：https://echarts.apache.org/examples/zh/editor.html?c=line-smooth







### 标题和x轴样式

1. 标题:`title`
2. x轴:`xAxis`

```js
 xAxis: {
      type: "category",
      // data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
      data: data.map((item) => item.month),
      //设置坐标轴轴线样式（颜色和虚线）
      axisLine: {
        lineStyle: {
          color: "#ccc",
          type: "dashed",
        },
      },
      //坐标轴标签的颜色
      axisLabel: {
        color: "#999",
      },
    },
```



### y轴分割线

1. y轴:`yAxis.splitLine`

```js
yAxis: {
      type: "value",
      //分隔线的样式
      splitLine: {
        lineStyle: {
          type: "dashed",
        },
      },
    },
```



### 提示框组件

1. 提示框:`tooltip`

```js
//鼠标移动出现提示
tooltip: {
      trigger: 'axis',
    },
```



### 绘图网络

1. 直角坐标系绘图网格:`grid` 

```js
//设置绘图网格
grid: {
  //网格距离上方距离
      top: "20%",
    },
```



### 折线的颜色

1. 调色盘:`color`

   ```js
   //设置折线图为渐变颜色
   color: [
         {
           type: "linear",
           x: 0,
           y: 0,
           x2: 1,
           y2: 0,
           colorStops: [
             {
               offset: 0,
               color: "#499FEE", // 0% 处的颜色
             },
             {
               offset: 1,
               color: "#5D75F0", // 100% 处的颜色
             },
           ],
         },
       ],
   ```

   



### 数据项配置

1. 数据项:`series`
   1. 标记:`symbol`
   
   2. 线段样式:`lineStyle`
   
   3. 区域样式:`areaStyle`
   
      1. 渐变色设置:
   
         **a ,b,c,d为0，1**
         **a:1 arr中的颜色右到左**
         **c:1 arr中的颜色左到右**
         **b:1 arr中的颜色下到上**
         **d:1 arr中的颜色上到下**
   
         *Tips:如果为0的话则相反*
   
         ```js
         new echarts.graphic.LinearGradient(a,b,c,d,arr)
         ```
   
         

```js
series: [
      {
        data: data.map((item) => item.salary),
        type: "line",
        smooth: true,
        lineStyle: {
          width: 6,
        },
        //标记的大小
        symbolSize: 10,
        //设置区域的渐变颜色
        areaStyle: {
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
            {
              offset: 0,
              color: "#499FEE",
            },
            {
              offset: 0.8,
              color: "rgba(255,255,255,0.2)",
            },
            {
              offset: 1,
              color: "rgba(255,255,255,0)",
            },
          ]),
        },
      },
    ],
```



### 数据整合

1. 结合最开始获取到的数据渲染页面







### 薪资走势

这一节咱们完成了薪资走势的设置,核心步骤:

1. 找示例
2. 根据文档调整配置
3. 整合到页面中
4. 整合在线数据







# 薪资分布

> 接下来是薪资分布

![image-20220515163913480](assets/image-20220515163913480.png)

### 基本套路:

1. 官网找到相似案例
2. 按照需求定制图表

相似案例：https://echarts.apache.org/examples/zh/editor.html?c=pie-borderRadius





### 标题和图例位置

1. 标题:`title`

2. 图例:`legend`

   ```js
   //设置图表的标题和字体样式 
   title: {
         text: "班级薪资分布",
         top: 15,
         left: 10,
         textStyle: {
           fontSize: 16,
         },
       },
   //图表的图例      
   legend: {
         bottom: "6%",
         left: "center",
       },
   ```

   





### 修改配色

1. 调色盘:`color`	

```js
color: ["#FDA224", "#5097FF", "#3ABCFA", "#34D39A"],
```



### 修改图表样式

1. 数据项:`series`
   1. 半径:`radius`
   2. 中心点坐标:`center`
   3. 区域样式:`itemStyle`
   4. 文本:`label`
   5. 刻度线:`labelLine`

```js
series: [
      {
        name: "班级薪资分布",
        //设置图标样式
        type: "pie",
        //半径:radius
        radius: ["50%", "64%"],
        //中心点坐标
        center: ["50%", "45%"],
        //区域样式
        itemStyle: {
          borderRadius: 10,
          borderColor: "#fff",
          borderWidth: 2,
        },
        //文本
        label: {
          show: false,
          position: "center",
        },
        //刻度线
        labelLine: {
          show: false,
        },
        // data: [
        //   { value: 1048, name: '1万以下' },
        //   { value: 235, name: '1万-1.5万' },
        //   { value: 735, name: '1.5万-2万' },
        //   { value: 580, name: '2万以上' },
        // ],
        data: data.map((item) => {
          return {
            value: item.g_count + item.b_count,
            name: item.label,
          };
        }),
      },
    ],
```



### 整合

1. 整合到项目
1. 结合最开始获取到的数据渲染页面



# 每组薪资

> 接下来是每组薪资

![image-20220516010309907](assets/image-20220516010309907.png)

### 基本图表整合

#### 基本套路

1. 官网找到相似案例
2. 按照需求定制图表

相似案例：https://echarts.apache.org/examples/zh/editor.html?c=bar-simple





#### 图表区域

1. 区域:`grid` 

```js
grid: {
      left: 70,
      top: 30,
      right: 30,
      bottom: 50,
    },
```



#### 配置x轴, y轴

1. x轴:`xAxis`

```js
xAxis: {
      type: "category",
      // data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'ooi'],
      data: data[1].map((item) => item.name),
      axisLine: {
        lineStyle: {
          color: "#ccc",
          type: "dashed",
        },
      },
      axisLabel: {
        color: "#999",
      },
    },
```



1. y轴:`yAxis`

```js
yAxis: {
      type: "value",
      splitLine: {
        lineStyle: {
          type: "dashed",
        },
      },
    },
```



#### 配置tooltip, color

1. 提示框:`tooltip`

```
tooltip: {
      trigger: "item",
    },
```



1. 颜色:`color`

```js
color: [
      {
        type: "linear",
        x: 0,
        y: 0,
        x2: 0,
        y2: 1,
        colorStops: [
          {
            offset: 0,
            color: "#34D39A", // 0% 处的颜色
          },
          {
            offset: 1,
            color: "rgba(52,211,154,0.2)", // 100% 处的颜色
          },
        ],
      },
      {
        type: "linear",
        x: 0,
        y: 0,
        x2: 0,
        y2: 1,
        colorStops: [
          {
            offset: 0,
            color: "#499FEE", // 0% 处的颜色
          },
          {
            offset: 1,
            color: "rgba(73,159,238,0.2)", // 100% 处的颜色
          },
        ],
      },
    ],
```

#### 配置图表样式

```js
series: [
      {
        // data: [12200, 17932, 13901, 13934, 21290, 23300, 13300, 13320],
        data: data[1].map((item) => item.hope_salary),
        type: "bar",
        name: "期望薪资",
      },
      {
        // data: [22820, 19932, 16901, 15934, 31290, 13300, 14300, 18320],
        data: data[1].map((item) => item.salary),
        type: "bar",
        name: "就业薪资",
      },
    ],
```



### 交互效果:

**更新图表:**

* 重新调用`setOption`方法传入数据即可

**需求:**

1. 整合到项目中
2. 点击**按钮**切换
   1. 高亮显示
   2. 渲染的数据


**步骤:**

1. 为按钮绑定**点击**事件
   1. 自己高亮:添加`btn-blue`
   2. 其他移除:移除`btn-blue`

2. 替换`options`中的数据
   1. 点击按钮时,获取内部的内容`innerHTML`
   2. 内容作为索引,获取对应的数据
   3. 更新`option`

3. 重新调用`setOption`方法传入数据即可



```js
btns.addEventListener("click", (e) => {
  //事件委托
    if (e.target.tagName === "BUTTON") {
      //排他思想
      btns.querySelector(".btn-blue")?.classList.remove("btn-blue");
      e.target.classList.add("btn-blue");
      //确定获取的是哪一组的数据
      const group = e.target.innerText;
      // 切换数据
      option.xAxis.data = data[group].map((item) => item.name);
      option.series[0].data = data[group].map((item) => item.hope_salary);
      option.series[1].data = data[group].map((item) => item.salary);
      myEchart.setOption(option);
    }
  });
```



### 每组薪资

搞定了每组薪资,套路:

1. 基础图表
2. 整合到项目中
3. 转化数据为需要的格式





### 注意:

1. `querySelectorAll`返回的是伪数组-->forEach?
   1. 原型上有`forEach`方法
   2. 没有其他的数组方法
2. `innerHTML`拿到的不是索引多了一些内容?
   1. `.trim()`
   2. 格式化之后,多了换行和缩进





# 男女薪资分布

> 然后是男女薪资分布

![image-20220516010447608](assets/image-20220516010447608.png)

### 基本套路

1. 官网找到相似案例
   1. 多个标题
   2. 多个饼图

2. 按照需求定制图表

相似案例：https://echarts.apache.org/examples/zh/editor.html?c=pie-simple





### 步骤:

1. 准备两个饼图数据，去掉图例
2. 添加标题(数组的方式添加多个)
3. 调整配色:color 配色
4. 结合接口数据-完成渲染



# 籍贯分布

> 最后是籍贯分布

![image-20220516010919943](assets/image-20220516010919943.png)



### 基本套路

1. 更为复杂的图表可以参考社区

2. echarts社区：https://www.makeapie.cn/echarts

3. 社区模板代码地址：https://www.makeapie.cn/echarts_content/xr1W9m5LOG.html

   注意：直接 CV 下面的即可

```javascript
const initMapChart = (provinceData) => {
  const myEchart = echarts.init(document.querySelector('#map'))
  const dataList = [
    { name: '南海诸岛', value: 0 },
    { name: '北京', value: 0 },
    { name: '天津', value: 0 },
    { name: '上海', value: 0 },
    { name: '重庆', value: 0 },
    { name: '河北', value: 0 },
    { name: '河南', value: 0 },
    { name: '云南', value: 0 },
    { name: '辽宁', value: 0 },
    { name: '黑龙江', value: 0 },
    { name: '湖南', value: 0 },
    { name: '安徽', value: 0 },
    { name: '山东', value: 0 },
    { name: '新疆', value: 0 },
    { name: '江苏', value: 0 },
    { name: '浙江', value: 0 },
    { name: '江西', value: 0 },
    { name: '湖北', value: 0 },
    { name: '广西', value: 0 },
    { name: '甘肃', value: 0 },
    { name: '山西', value: 0 },
    { name: '内蒙古', value: 0 },
    { name: '陕西', value: 0 },
    { name: '吉林', value: 0 },
    { name: '福建', value: 0 },
    { name: '贵州', value: 0 },
    { name: '广东', value: 0 },
    { name: '青海', value: 0 },
    { name: '西藏', value: 0 },
    { name: '四川', value: 0 },
    { name: '宁夏', value: 0 },
    { name: '海南', value: 0 },
    { name: '台湾', value: 0 },
    { name: '香港', value: 0 },
    { name: '澳门', value: 0 },
  ]
  let option = {
    title: {
      text: '籍贯分布',
      top: 10,
      left: 10,
      textStyle: {
        fontSize: 16,
      },
    },
    tooltip: {
      trigger: 'item',
      formatter: '{b}: {c} 位学员',
      borderColor: 'transparent',
      backgroundColor: 'rgba(0,0,0,0.5)',
      textStyle: {
        color: '#fff',
      },
    },
    visualMap: {
      min: 0,
      max: 6,
      left: 'left',
      bottom: '20',
      text: ['6', '0'],
      inRange: {
        color: ['#ffffff', '#0075F0'],
      },
      show: true,
      left: 40,
    },
    geo: {
      map: 'china',
      roam: false,
      zoom: 1.0,
      label: {
        normal: {
          show: true,
          fontSize: '10',
          color: 'rgba(0,0,0,0.7)',
        },
      },
      itemStyle: {
        normal: {
          borderColor: 'rgba(0, 0, 0, 0.2)',
          color: '#e0ffff',
        },
        emphasis: {
          areaColor: '#34D39A',
          shadowOffsetX: 0,
          shadowOffsetY: 0,
          shadowBlur: 20,
          borderWidth: 0,
          shadowColor: 'rgba(0, 0, 0, 0.5)',
        },
      },
    },
    series: [
      {
        name: '籍贯分布',
        type: 'map',
        geoIndex: 0,
        data: dataList,
      },
    ],
  }
  myEchart.setOption(option)
}


```



### 数据转换

1. 将接口返回的数据设置到`dataList`,重新渲染即可

![image-20220806181731859](assets/image-20220806181731859.png)



### 籍贯分布

图表是直接c+v出来的,本节的核心点是:

1. 数据转换
   1. 工作的时候比较常见
   2. 后端返回给前端的数据格式,不一定满足要求
   3. 经常需要转换数据
      1. 数组的新方法
         1. forEach
         2. map
         3. filter
         4. find
         5. every
         6. some
         7. indexof
         8. reduce
         9. .....






## 总结 和 作业

> 今天的重点内容 和 作业

### 总结:

























