// 上面这个代码处理过度动画（默认加上不用管）
document.addEventListener("DOMContentLoaded", () => {
  setTimeout(() => {
    document.body.classList.add("sidenav-pinned");
    document.body.classList.add("ready");
  }, 200);
});

// 每个页面都会导入该js 统一的设置写这里即可

//封装轻弹窗
const toastBox = document.querySelector("#myToast");
const toast = new bootstrap.Toast(toastBox);
const tip = (msg) => {
  toastBox.querySelector(".toast-body").innerHTML = msg;
  toast.show();
};

//设置axios请求的基地址
axios.defaults.baseURL = "http://ajax-api.itheima.net";

//封装axios的get方法
const get = (url, data) => {
  return axios({
    method: "get",
    url,
    params: data,
  });
};

//封装axios的post方法
const post = (url, data) => {
  return axios({
    method: "post",
    url,
    data,
  });
};

//axios的请求拦截器
axios.interceptors.request.use(
  function (config) {
    console.log(config);
    config.headers.Authorization = localStorage.getItem("token");
    // 在发送请求之前做些什么
    return config;
  },
  function (error) {
    tip("我错了");
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么
    return response.data;
  },
  function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    console.dir(error);
    if (error.response.status === 401) {
      location.href = "./login.html";
    }
    return Promise.reject(error);
  }
);
